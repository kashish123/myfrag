package com.example.hp.fragmentproject.Comparators;

import com.example.hp.fragmentproject.Student;

import java.util.Comparator;

/**
 * Created by HP on 10/14/2015.
 */
public class ByRollNo implements Comparator<Student> {
    @Override
    public int compare(Student lhs, Student rhs) {
        if (lhs.getRollNo() > rhs.getRollNo())
            return 1;
        else if (lhs.getRollNo() < rhs.getRollNo())
            return -1;
        else
            return 0;
    }
}



