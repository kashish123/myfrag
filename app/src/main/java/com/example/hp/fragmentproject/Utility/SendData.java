package com.example.hp.fragmentproject.Utility;

import com.example.hp.fragmentproject.Student;

/**
 * Created by HP on 10/14/2015.
 */
public interface SendData {

    void sendData(Student obj, int request);
}
