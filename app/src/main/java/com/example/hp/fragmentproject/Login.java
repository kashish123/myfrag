package com.example.hp.fragmentproject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hp.fragmentproject.Utility.Constants;

public class Login extends Activity implements View.OnClickListener,Constants {


    TextView nametext, passwordtext;
    String name, password;
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        Button login = (Button) findViewById(R.id.loginbutton);
        Button exit = (Button) findViewById(R.id.exitButton);
        nametext = (TextView) findViewById(R.id.name);
        passwordtext = (TextView) findViewById(R.id.password);
        sharedPreferences = getSharedPreferences("mydata", Context.MODE_PRIVATE);


        if ((USERNAME.equals(sharedPreferences.getString("name", "")) && (PASSWORD.equals(sharedPreferences.getString("password", null))))) {
            Intent intentObj = new Intent(this, MyFragment.class);
            startActivity(intentObj);
            finish();
        }

        CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);
        final EditText password = (EditText) findViewById(R.id.password);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    password.setInputType(TYPE_TEXT_VARIATION_INVISIBLE);
                }
            }
        });


        login.setOnClickListener(this);
        exit.setOnClickListener(this);






    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.loginbutton:
                name = nametext.getText().toString();
                password = passwordtext.getText().toString();
                if (name.equals(USERNAME) && password.equals(PASSWORD)) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("name", USERNAME);
                    editor.putString("password", PASSWORD);
                    editor.commit();
                    Intent intentObj = new Intent(this, MyFragment.class);
                    startActivity(intentObj);
                    finish();
                } else {
                    Toast.makeText(this, "wrong data entered", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.exitButton:
                ActivityCompat.finishAffinity(this);
                break;
        }
    }
    }

