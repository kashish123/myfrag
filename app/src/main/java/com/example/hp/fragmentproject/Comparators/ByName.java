package com.example.hp.fragmentproject.Comparators;

import com.example.hp.fragmentproject.Student;

import java.util.Comparator;

/**
 * Created by HP on 10/14/2015.
 */
public class ByName implements Comparator<Student> {
    @Override
    public int compare(Student lhs, Student rhs) {
        String firstName = lhs.getStudentName();
        String secondName = rhs.getStudentName();
        int compareResult = firstName.compareToIgnoreCase(secondName);
        if (compareResult > 0) {
            return 1;
        } else {
            return -1;

        }
    }
}