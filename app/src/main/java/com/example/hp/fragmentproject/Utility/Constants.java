package com.example.hp.fragmentproject.Utility;

/**
 * Created by HP on 10/14/2015.
 */
public interface Constants {


    // DIALOG BOX POSITIONS
    int EDIT = 0;
    int DISPLAY = 1;
    int DELETE = 2;

    int NUM_OF_COLMS = 2;

    //NAVIGATION LISTITEM POSITION
    int ADD = 0;
    int LOGOUT = 1;

    int ADD_STUDENT = 1;
    int DELETE_STUDENT = 2;
    int UPDATE_STUDENT = 10;
    int UPDATE = 8;
    int CLOSE = 0;

    int VIEW_SPECIFIC_CODE = 5;
    int VIEW_ALL_CODE = 9;
    int ADD_DETAILS_CODE = 1;
    int EDIT_DETAILS_CODE = 100;
    int TYPE_TEXT_VARIATION_INVISIBLE = 129;









    // TOAST
    String ADD_DETAIL_KEY = "'s details added";
    String UPDATE_DETAIL_KEY = "'s details updated";
    String UNSUCCESSFUL_TEXT = "Unsuccessful";
    String INCORRECT_CREDENTIALS = "Incorrect Credentials";
    String ENTER_DATA_KEY = "Enter the complete data";

    String DATA_DELETED = "Data deleted";

    // TEXTVIEW TITLE OF FRAGMENT
    String EDIT_DATA_TEXT = "Edit the data of ID : ";
    String DISPLAY_DATA_TEXT = "Data of ID : ";
    String ENTER_DATA_TEXT = "Enter the new data ";

    //SHARED PREFERENCES
    String USERNAME = "kashish";
    String PASSWORD = "pass";
    String SHARED_PREFERENCES_NAME = "My_data";
}
