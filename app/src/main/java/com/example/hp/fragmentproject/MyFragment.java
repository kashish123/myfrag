package com.example.hp.fragmentproject;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.hp.fragmentproject.Comparators.ByName;
import com.example.hp.fragmentproject.Comparators.ByRollNo;
import com.example.hp.fragmentproject.Entity.MyAdapter;
import com.example.hp.fragmentproject.Entity.MyDialog;
import com.example.hp.fragmentproject.MyDataBase.DBController;
import com.example.hp.fragmentproject.Navigation.DrawerItem;
import com.example.hp.fragmentproject.Navigation.MyDrawerAdapter;
import com.example.hp.fragmentproject.Utility.CommonUtils;
import com.example.hp.fragmentproject.Utility.Constants;
import com.example.hp.fragmentproject.Utility.MyDialogListener;
import com.example.hp.fragmentproject.Utility.ReceiveData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class MyFragment extends AppCompatActivity implements View.OnClickListener, ReceiveData, Constants, MyDialogListener, CommonUtils.OnItemClickListener, AdapterView.OnItemClickListener {

    MyAdapter adapter;
    Intent intent;
    LinkedList<Student> studentsList;
    SharedPreferences sharedPreferences;
    int positionOfItem;
    Spinner spinner;
    RecyclerView recyclerView;
    Student studentObj;
    FragmentManager fragmentManager;
    AddStudent addStudent = new AddStudent();
    RelativeLayout myLayout, fragmentLayout;
    Button listButton, gridButton;

    DrawerLayout drawerLayout;
    ListView drawerList;
    ActionBarDrawerToggle drawerToggle;
    CharSequence drawerTitle;
    CharSequence title;
    MyDrawerAdapter customAdapter;
    List<DrawerItem> dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_fragment);

        studentsList = new LinkedList<>();
        adapter = new MyAdapter(this, studentsList, R.layout.myrowlayout);
        sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);

        listButton = (Button) findViewById(R.id.listButton);
        gridButton = (Button) findViewById(R.id.gridButton);

        myLayout = (RelativeLayout) findViewById(R.id.myrelativelayout);
        fragmentLayout = (RelativeLayout) findViewById(R.id.fragmentlayout);
        spinner = (Spinner) findViewById(R.id.spinner);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.spinnerList));
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);

        /* Sorting according to item selected in spinner*/
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String sort = spinner.getSelectedItem().toString();
                if (sort.equals("Roll No")) {
                    Collections.sort(studentsList, new ByRollNo());
                    adapter.notifyDataSetChanged();
                } else if (sort.equals("Name")) {
                    Collections.sort(studentsList, new ByName());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        dataList = new ArrayList<>();
        title = drawerTitle = getTitle();
        drawerList = (ListView) findViewById(R.id.list_slidermenu);

        dataList.add(new DrawerItem("ADD DETAILS"));
        dataList.add(new DrawerItem("LOGOUT"));
        customAdapter = new MyDrawerAdapter(this, R.layout.custom_drawer_item, dataList);
        drawerList.setAdapter(customAdapter);
        drawerList.setOnItemClickListener(this);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(title);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(drawerTitle);
                invalidateOptionsMenu();
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(drawerToggle);

        listButton.setOnClickListener(this);
        gridButton.setOnClickListener(this);
        adapter.setOnItemClickListener(this);
        new AsyncTasks().execute(VIEW_ALL_CODE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.listButton:

                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                break;
            case R.id.gridButton:

                recyclerView.setLayoutManager(new StaggeredGridLayoutManager(NUM_OF_COLMS, StaggeredGridLayoutManager.VERTICAL));
                break;
        }
    }


    @Override
    public void recieveData(Student obj, int request) {
        unlockNavigationBar();
        getFragmentManager().beginTransaction().remove(addStudent).commit();
        myLayout.setVisibility(View.VISIBLE);
        studentObj = obj;
        switch (request) {
            case ADD_DETAILS_CODE:
                new AsyncTasks().execute(ADD_STUDENT);
                studentsList.add(studentObj);
                Toast.makeText(this, studentObj.getStudentName() + ADD_DETAIL_KEY, Toast.LENGTH_SHORT).show();
                break;

            case EDIT_DETAILS_CODE:
                new AsyncTasks().execute(UPDATE_STUDENT);
                Toast.makeText(this, studentObj.getStudentName() + UPDATE_DETAIL_KEY, Toast.LENGTH_SHORT).show();
                break;

            case CLOSE:
                break;
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case ADD:
                callFragment();
                lockNavigationBar();
                break;

            case LOGOUT:
                sharedPreferences.edit().clear().commit();
                intent = new Intent(this, Login.class);
                startActivity(intent);
                finish();
                break;
        }
    }


    class AsyncTasks extends AsyncTask<Integer, Student, Long> {
        private DBController dbControllerobj;
        private MyAdapter myAdapter;
        private int choice;
        ProgressDialog progressDialog;
        private long result;

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(MyFragment.this);
            progressDialog.show();
            progressDialog.setMessage("data is updating");
            dbControllerobj = new DBController(MyFragment.this);
            myAdapter = (MyAdapter) recyclerView.getAdapter();
        }


        @Override
        protected void onProgressUpdate(Student... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected Long doInBackground(Integer... params) {
            this.choice = params[0];
            switch (choice) {
                case VIEW_ALL_CODE:
                    studentsList = dbControllerobj.viewAll(studentsList);
                    break;

                case ADD_STUDENT:
                    result = dbControllerobj.insert(studentObj);
                    studentObj.rollNo = (int) result;
                    break;

                case DELETE_STUDENT:
                    result = dbControllerobj.delete(positionOfItem);
                    break;

                case UPDATE:
                    studentObj = dbControllerobj.view(positionOfItem);
                    break;

                case UPDATE_STUDENT:
                    result = dbControllerobj.update(studentObj);
                    break;

                case VIEW_SPECIFIC_CODE:
                    studentObj = dbControllerobj.view(positionOfItem);
                    break;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            switch (choice) {
                case VIEW_SPECIFIC_CODE:
                    addStudent.sendData(studentObj, VIEW_SPECIFIC_CODE);
                    break;
                case UPDATE:
                    addStudent.sendData(studentObj, EDIT_DETAILS_CODE);
                    break;
            }
            if (result > 0) {
                studentsList = dbControllerobj.viewAll(studentsList);
            } else if (result == -1) {
                Toast.makeText(MyFragment.this, UNSUCCESSFUL_TEXT, Toast.LENGTH_SHORT).show();
            }
            if (spinner.getSelectedItem().toString().equals("Name")) {
                Collections.sort(studentsList, new ByName());
            }
            myAdapter.notifyDataSetChanged();
            progressDialog.dismiss();
        }

    }


    @Override
    public void onClick(int position) {
        switch (position) {
            case EDIT:
                callFragment();
                lockNavigationBar();
                new AsyncTasks().execute(UPDATE);
                break;

            case DISPLAY:
                callFragment();
                lockNavigationBar();
                new AsyncTasks().execute(VIEW_SPECIFIC_CODE);
                break;

            case DELETE:
                new AsyncTasks().execute(DELETE_STUDENT);
                new AsyncTasks().execute(VIEW_ALL_CODE);
                Toast.makeText(this, DATA_DELETED, Toast.LENGTH_SHORT).show();
                break;
        }
    }


    @Override
    public void onItemClick(int position) {
        MyDialog dialogObj = new MyDialog();
        dialogObj.setDialogListener(MyFragment.this);
        positionOfItem = position;
        dialogObj.show(getFragmentManager(), "dialog");
    }


    void lockNavigationBar() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerLayout.closeDrawer(drawerList);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
    }


    void unlockNavigationBar() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }


    void callFragment() {
        myLayout.setVisibility(View.GONE);
        fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentlayout, addStudent, "start");
        fragmentTransaction.commit();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return false;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setTitle(CharSequence mtitle) {
        title = mtitle;
        getSupportActionBar().setTitle(title);
    }
}