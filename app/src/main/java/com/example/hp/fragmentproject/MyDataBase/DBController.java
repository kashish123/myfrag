package com.example.hp.fragmentproject.MyDataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.hp.fragmentproject.Student;

import java.util.LinkedList;

/**
 * Created by HP on 10/14/2015.
 */
public class DBController implements DBConstants {

    SQLiteDatabase db;
    DBHandler dbHelperObj;

    public DBController(Context context) {
        dbHelperObj = new DBHandler(context, DATABASE_NAME, null, VERSION);
    }

    public long insert(Student student) {
        db = dbHelperObj.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME, student.getStudentName());
        contentValues.put(COLUMN_ADDRESS, student.getAddress());
        contentValues.put(COLUMN_MOBILE, student.getMobile());
        long id = db.insert(TABLE, null, contentValues);
        db.close();
        return id;
    }

    public Student view(int id) {
        db = dbHelperObj.getWritableDatabase();
        Cursor cursor = db.query(TABLE, null, COLUMN_ROLLNO + "=?", new String[]{String.valueOf(id)}, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        Student student = new Student();
        student.setRollNo(Integer.parseInt(cursor.getString(0)));
        student.setStudentName(cursor.getString(1));
        student.setAddress(cursor.getString(2));
        student.setMobile(cursor.getString(3));
        close(db);
        return student;
    }

    public LinkedList<Student> viewAll(LinkedList<Student> students) {
        students.clear();
        db = dbHelperObj.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT_QUERY, null);
        Student student;
        if (cursor.moveToFirst()) {
            do {
                student = new Student();
                student.setRollNo(Integer.parseInt(cursor.getString(0)));
                student.setStudentName(cursor.getString(1));
                student.setAddress(cursor.getString(2));
                student.setMobile(cursor.getString(3));
                students.add(student);
            } while (cursor.moveToNext());
        }
        close(db);
        return students;
    }

    public long update(Student student) {
        db = dbHelperObj.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME, student.getStudentName());
        contentValues.put(COLUMN_ADDRESS, student.getAddress());
        contentValues.put(COLUMN_MOBILE, student.getMobile());
        long i = db.update(TABLE, contentValues, COLUMN_ROLLNO + "=?", new String[]{String.valueOf(student.getRollNo())});
        close(db);
        return i;
    }

    public long delete(int id) {
        db = dbHelperObj.getWritableDatabase();
        long i = db.delete(TABLE, COLUMN_ROLLNO + "=?", new String[]{String.valueOf(id)});
        close(db);
        return i;
    }

    public void close(SQLiteDatabase db) {
        db.close();
    }
}










